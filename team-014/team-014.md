# Model to Text Transformation

---

O presente documento foi desenvolvido no âmbito da unidade curricular de Engenharia de Domínio (EDOM),
no primeiro ano do Mestrado em Engenharia Informática (MEI), no ramo de Engenharia de Software, no Instituto Superior 
de Engenharia do Porto (ISEP).


O projeto foi desenvolvido por: 
* Renato Sousa - 1150509
* João Amorim - 1150390
* Carla Sampaio - 1180106
* João Westerberg - 1150447


O projeto desenvolvido para a realização deste documento incide na documentação de transformações de modelo para texto
e a apresentação de um caso de estudo em alternativa ao software utilizado em aula. Os temas que iremos abordar são:


- [Transformações de Modelo](#transformações-de-modelo)
- [Linguagens](#linguagens)
- [Transformação M2T](#transformação-m2t)
- [JET](#jet)
- [XPAND](#xpand)
- [Acceleo](#acceleo)




## Transformações de Modelo
Em diversos cenários, instâncias de modelos de domínio em EMF precisam de ser transformadas em outros modelos ou em 
representações textuais. Portanto, existem diversas frameworks e ferramentas dedicadas que permitem desenvolver 
transformações de modelo para outro, e transformações de um modelo para texto. Com as transformações de modelo a 
modelo/transformação de modelo para texto, os modelos podem ser transformados em uma representação textual, por exemplo,
código-fonte de uma linguagem específica. No EMF, vários frameworks estão disponíveis para suportar esse caso de uso




## Linguagens
Sendo a transformação de modelos um ponto fundamental do Model Driven Design (MDD), foram desenvolvidas várias linguagens de transformação especializadas. As linguagens de transformação de modelo podem ser categorizadas de acordo com várias categorias.


É importante definir  que tipo de transformação pretendemos realizar: tipo de fonte
e alvo da transformação (por exemplo: modelo a modelo, modelo a texto, etc.), se
o tipo é vertical (por exemplo, alterações no modelo, geração de código etc.) ou horizontal (por exemplo: refactorização, otimização, etc.), atributos de qualidade das ferramentas de transformação (por exemplo: nível de automação,testabilidade, usabilidade, etc.), representação do programa de transformação(declarativo versus operacional).


Projeto de modelagem Eclipse coleta a transformação de modelo de código aberto mais popular
ferramentas para MDD. Seu núcleo é o Eclipse Modeling Framework, que fornece ferramentas
para aplicativos de modelagem de construção. O projeto contém ferramentas para Modelar para Modelar
transformações, e. ATL, VIATRA2; e para transformações Model to Text
por exemplo. JET, Xpand.


Também estão disponíveis linguagens de pesquisa na Modelagem Eclipse
projet: Epsilon é uma família de idiomas que contém várias linguagens específicas de tarefas
parte superior do Epsilon Object Language, Henshin, que é uma transformação de alto desempenho
linguagem de qual notação baseia em transformações de gráfico atribuídas.




## Transformação M2T
As linguagens de transformação de modelos para texto visam melhorar o desenvolvimento do gerador de código, abordando as desvantagens anteriormente declaradas dos geradores de código baseados em GPL.


![alt text](Imagem1.png "Transformação M2T")

Tipicamente as linguagens para transformação M2T utilizam uma abordagem em *templates*, na qual o código a ser gerado é dividido em código estático, ou seja independente do conteúdo do modelo, e código dinâmico que por sua vez varia consoante o modelo recebido como input.

## JET
O Jet, conhecido principalmente como Java Emitter Template, foi uma das primeiras abordagens para o desenvolvimento de geração de código para modelos baseados em EMF. Esta linguagem de transformação de modelos para texto tem várias vantagens tais como:


1. Não está limitado a modelos baseados em EMF;
2. Todos os objetos Java são transformáveis para texto;
3. Todos os templates são transformados em puro código Java pronto a ser executado.




## XPAND
O Xpand é uma linguagem de  transformação de modelos com uma tipologia estática, que surgiu  durante o desenvolvimento do projeto openArchitectureWare. Posteriormente foi migrado para Eclipse, tornando-use um componente isolado deste mesmo IDE sob a forma de Projeto de Modelação. Tem diversas vantagens tais como:

1. Invocação de modelos polimórficos;
2. Programação Orientada a Aspectos;
3. Extensões funcionais;
4. Abstração do sistema do tipo flexível;
5. Transformação e validação de modelos;

## Epsilon Generation Language (EGL)
Epsilon Generation Language (EGL) é uma linguagem de transformação de modelo para texto (M2T) que é um componente de uma cadeia de ferramentas de gestão de modelos. As características distintas do EGL são descritas no seu design inovador que herda vários conceitos de linguagem e características lógicas de uma linguagem de modificação e navegação de modelo base. Apresenta diversas vantagens tais como:

1. Desacoplar o conteúdo do destino (pode ser usado para gerar texto para ficheiros, para a área de transferência ou como uma linguagem de script do lado do servidor, etc);
2. Usar Templates(com parâmetros) de outros modelos;
3. Definir e chamar submodelos;
4. ???Permite misturar código gerado com o não gerado;Abstração do sistema do tipo flexível;
5. Transformação e validação de modelos;
6. Modelos de chamada (com parâmetros) de outros modelos
7. Mix gerado com código escrito à mão???

## Acceleo

O Acceleo é uma implementação pragmática do padrão MOF Model to Text Language (MTL) do Object Management Group (OMG). 

Os plug-ins fornecidos apresentam as características de serem fáceis de usar e possuírem uma simples curva de 
aprendizagem permitindo desenvolvimento do motor de geração de código recebendo como input modelos EMF e produzindo como output código para qualquer linguagem.

1. Fornece uma versão pragmática do padrão de transformação M2T do OMG para modelos baseados em EMF.
2. Fornece suporte completo ao OCL para consultar modelos.
3. Oferece uma linguagem baseada em modelo para definir modelos de geração de código.


## Tutorial  EGL (How-to)

Este exemplo demonstra o uso do EGL para gerar código HTML a partir de um documento XML, este encontra-se representado na figura abaixo.
Não esquecer que o EGL suporta diferentes tipos de modelos (EMF, UML) sendo que neste tutorial iremos focar o modelo XML.

![alt text](ficheiroInicial.PNG "EGL Import File")

Vamos gerar um ficheiro HTML para cada elemento <book> que tenha um atributo público definido como true. Abaixo podemos ver o modelo EGL que foi utilizado para gerar um ficheiro HTML a partir de um único elemento <book>. 

![alt text](HtmlPerBook.PNG "Generate HTML 1 EGL")

O modelo acima pode gerar um arquivo HTML de um elemento <book>. Para executar este modelo em todos os elementos <book> em qualquer lugar no documento XML e gerar arquivos HTML com o nome apropriado, precisamos de usar um programa de coordenação EGX como o que está representado abaixo (main.egx). A regra Book2Page do programa EGX transformará todos os elementos <book> (t_book) que satisfizerem a validação declarada (ter um atributo público definido como true), num ficheiro de destino, usando o modelo especificado (book2page.egl). Além disso, o programa EGX especifica uma regra Library2Page, que gera um ficheiro HTML (neste caso o índice) para cada elemento <library> no documento.

![alt text](Book2PageRule.PNG "EGL Rules 1")


![alt text](Library2Page.PNG "EGL Rules 2")

Para completar, o resto do código do template library2page.egl aparece abaixo.

![alt text](Library2PageEGL.PNG "Generate HTML 2 EGL")

Podemos correr pelo Eclipse, como EGL Template, seguindo as seguintes configurações:

![alt text](RunAs1.PNG "Run As Step 1")

![alt text](RunAsModel2.PNG "Run As Step 2")

![alt text](RunAsEditModel.PNG "Run As Step 3")

Ou como um simples programa Java*:

![alt text](RunFromJava.PNG "Run As Java Application")

*Caso quiséssemos correr fora do Eclipse, apenas teríamos de converter para Maven o projecto e alterar o pom com a execução desejada.

Como resultado final obtemos:

![alt text](FinalResult.PNG "EGL Result")

