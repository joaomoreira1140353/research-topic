# EDOM - Project Research Topic

# Trabalho realizado pelo o grupo 52
Hugo Soares 1140445
João Martins 1140552
Henrique Moura 1160085
João Moreira 1140353

# Model Validation - Validation in Epsilon

## Concern

Model validation, é o processo de determinar o grau em que o modelo é uma representação precisa do mundo real.
O objectivo da validação é quantificar a confiança na capacidade de previsão do modelo em comparação com dados experimentais. A abordagem da validação é medir a concordância entre as previsões do modelo e os dados experimentais de experimentos que foram correctamente desenvolvidos. A concordância é medida, por exemplo, quantificando a diferença (erro) entre os dados experimentais e o resultado do modelo. 


## Aproach

A estratégia adoptado pelo o grupo foi a validação do model através do Epsilon, ou seja, através da linguagem de validação do Epsilon (EVL).
No EVL as especificações de validação são organizados em módulos (EVLModule). O EvlModule estende EolLibraryModule, o que significa que ele pode conter operações definidas pelo utilizador e importar outros módulos da biblioteca EOL e módulos EVL. Além das operações, um módulo EVL também contém um conjunto de constantes agrupadas pelo contexto ao qual elas se aplicam.
Um "context" especifica o tipo de instâncias nas quais as "invariants" serão avaliadas. Cada "context" pode, opcionalmente, definir uma proteção que limita a aplicabilidade num subconjunto mais restrito de instâncias do tipo especificado. Assim, se a proteção falhar numa instância específica do tipo, nenhuma das suas "invariants" será avaliada.
Exemplo de definição de um context no EVL:

```
    @lazy)?
    context <name> {
        (guard (:expression)|({statementBlock}))?
        (invariant)* 
    }
```

Assim como no OCL, cada "invariant" de EVL define um nome e um corpo (check). No entanto, também pode opcionalmente definir um "guarda" (definido no supertipo abstracto de GuardedElement) que limita ainda mais a aplicabilidade a um subconjunto de instâncias do tipo definido pelo contexto envolvente.
As "constraints" no EVL são usadas para capturar erros críticos que invalidem o modelo. Constraint é uma subclasse de invariant, ou seja, herda todos os seus recursos.
Exemplo de definição de um invariant e de uma constraint no EVL:

```
    @lazy)?
    (constraint|critique) <name> {
        (guard (:expression)|({statementBlock}))?
        (check (:expression)|({statementBlock}))?
        (message (:expression)|({statementBlock}))? (fix)*
    }
```
A execução de um módulo EVL é separado em 4 fases:

    1. Antes que qualquer "invariant" seja avaliada, as pré-secções do módulo são executadas na ordem que foram especificadas.
    2. A ordem de execução de um módulo EVL segue um esquema de profundidade primeiro descendente, que respeita a ordem na qual os contextos e invariantes aparecem no módulo.
    3. Examina as validações e verifica quais as constraints que não foram satisfeitas e o utilizador recebe uma mensagem que cada um produziu. O utilizador pode selecionar uma ou mais opções de correcções disponiveis para serem executadas. A execução das correcções é realizado de maneira transacional que utiliza os recursos fornecidos. Com isto, impede que erros de tempo de execução obtidos durante a execução de uma correcção comprometam o modelo validado, deixando assim num estado de inconsestência.  
    4. O utilizador pode executar tarefas como por exemplo, produzir um resumo dos resultados obtidos no processo de validação.

## Case Study - 

O Epsilon é uma framework baseada em java que contém várias linguagens para tarefas como, validação geração de código e transformações de modelos. De seguida, vão ser enumeradas e brevemente explicadas as diferentes liguagens do Epsilon: 
		- Epsilon object Language (EOL): O principal objetivo da linguagem em análise é oferecer uma gestão de um modelo reutilizavel por linguagens de tarefas especificas;  
		- Epsilon validation Language (EVL): A EVL é responsável pelas validações de modelos quando se usa o epsilon.
		- Epsilon transformation language (ETL): O ETL responsavel pelas transformações de mapiamento de um modelo noutro modelo do epsilon. Uma transformação de mapiamento de um modelo visa a transformação do zero de um modelo para outro;  
		- Epsilon wizard language (EWL): O epsilon wizard language é uma linguagem de suporte para definir e executar uma transformação do tipo update em modelos de vários metamodelos;
		- Epsilon generation Language (EGL): O EGL é responsável pela geração de codigo executável, imagens ou até relatorios apartir de modelos;
		- Epsilon Comparison Language (ECL): O ECL é o que realiza a comparação entre dois modelos, que permitem verificar elementos em comum entre os mesmos. Os algoritmos de comparação são costumizados;  
		- Epsilon Merging Language (EML): A linguagem em análise trata da junção entre dois ou mais modelos.
No entanto, como referido no capítulo de "Concern" o estudo foi focado numa só linguagem de Epsilon, a epsilon validation Language (EVL). Neste capitulo vão ser demonstrados alguns dos casos de estudo executados para comparar epsilon a outras linguagens de validação. Durante a pesquisa feita sobre epsilon foram encontrados dois casos de estudo o "Intra-Model Consistency Checking Example" e o "Inter-Model Consistency Checking Example".
No Intra-Model Consistency Checking Example neste caso de estudo foram comparadas o EVL com o OCL num cenário comum. Neste caso o objetivo foi para apresentar a sintaxe da linguagem e os beneficios obtidos pela sua utilização. O cenário foi um padrão de Singleton, que é uma classe que só uma instancia é permitida. De modo a que fosse garantida a modulação correta de todos os singletons foram criadas invariants para que validassem se era de facto ou não um singleton. As invariants desenvolvidas foram três a "DefinesGetInstance", que cada classe tem de conter "getIntance()"; "GetInstanceIsStatic", que verifica se o método "getIntance()" é do tipo estático e "GetInstaceReturnsSame", que verifica se o retorno da método é a própria classe. 
As invariants usadas foram desenvolvidas em OCL e em EVL como meios de comparação. E o que o estudo comprovou foi que os construtores adicionais do EVL reduzem a repetição e assim melhoraram as restrições que são impostas a um modelo. Para além, dos pontos referidos anteriormente também é gerada uma mensagem com mais conteudo, se der erro.	
No caso de uso explicado anterirmente o EVL foi usado para verificar a consistência interna de um modelo uml, porém o EVL pode ser usado para verificar se entre dois modelos diferentes há contradição ou se estão incompletos. No caso de estudo seguinte pode-se verificar que um metamodel de nome "ProcessLang", que faz a captura de informação de forma herarquica. Neste caso o modelo "Process" vai ter um model e cada uma das instancias desse modelo têm um "ProcessPerformance", esse processo tem a restrição "maxAcceptableTime", na qual está verificado se o tempo maximo estipulado para cada processo não é ultrapassado pelo sumatório de tempo dos seus filhos. Esta restrição é obtida através das "PerformanceIsDefined" e "PerformanceIsValid" em EVL. Como exemplificado no treixo de código de seguida:
	
	context PM!Process { 
		constraint PerformanceIsDefined { 
			check { 
				PPM!ProcessPerformance. 
				allInstances.select(pt|pt.process = self); 
				 return processPerformances.size() = 1; 
				 }
				 .......
	}
	
		constraint PerformanceIsValid {
			 guard : self.satisfies("PerformanceIsDefined") and self.children.forAll (c|c.satisfies("PerformanceIsDefined"))
			 
			check { 
				var sum : Integer; 
				sum = self.children. 
				collect(c|c.getMaxAcceptableTime()) 
				.sum().asInteger(); 
				return self.getMaxAcceptableTime() >= sum; 
			}  
			...
			fix { 
			title : "Increase maxAcceptableTime to " + sum 
			do { 
			self.setMaxAcceptableTime(sum); 
			}

		}
A "PerformanceIsDefined" verifica quais as instâncias do tipo "ProcessPerformance" no modelo "ProcessPerformanceModel" que têm a referência do "process" igual à do processo analisado e se houverem são armazenadas na variavel "processPerformances". Na restrição "PerformanceIsValid" tem a parte da "guard", na qual vai restringir ainda mais, ou seja, o que a guard faz é a restrição só é aplicavel se a restrição "PerformanceIsDefined" for satisfeita. Se então se passar pelo guard, a restrição vai verificar se se o "maxAcceptableTime" do processo é igual ou maior à soma de todos os "maxAcceptableTime" dos filhos, note-se que se falhar a restrição o utiliuzador pode aplicar um fix, que o que faz é igualar o "maxAcceptableTime" do processo ao sumatório, corrigindo assim o erro.

## Compare the aproach

Nesta secção será comparada a utilização da EVL com a OCL por forma demontrastrar os beneficios da sua utilização para validação de modelos em modelos abastratos.

A EVL pode ser usada para especificar e avaliar restrições em modelos de metamodelos arbitrários e tecnologias de modelagem.

OCL (Object Constraint Language) é a linguagem padrão para capturar restrições em
linguagens de modelagem especificadas usando tecnologias de metamodelo orientadas a objetos. 
Enquanto que sua poderosa sintaxe permite aos utilizadores especificar restrições significativas e concisas, é puramente de natureza declarativa e sem efeitos colaterais introduz uma série de limitações no contexto
de um ambiente de gerenciamento de modelos contemporâneo.

Nas restrições em OCL, cada invariante é definida no contexto de uma meta-classe do metamodelo e especifica um nome e um corpo e avalia um resultado booleano, indicando se uma instância da meta-classe satisfaz a invariante ou não.
O corpo de cada invariante é avaliada para cada instância da meta-classe e os resultados são armazenados em um conjunto de 3 objetos <Element, Invariant, Boolean>. Cada trio captura o resultado booleano de a avaliação de um invariante em um elemento qualificado.

###Limitações da OCL

####Feedback limitado do utilizador:
O OCL não suporta a especificação de mensagens significativas que podem ser relatadas ao utilizador caso uma invariante não seja satisfeita para certos elementos. 
O feedback é limitado ao nome da invariante e à(s) instância(s) para as quais ele falhou.

####Não há suporte para avisos / críticas
Na OCL não existe tal distinção entre erros e avisos e em consequência, todos os problemas relatados são considerados erros. 
Isso adiciona uma carga adicional para identificar e priorizar questões de grande importância.

####Não há suporte para restrições dependentes
Cada invariante da OCL é uma unidade independente que não depende de outras invariantes.
Todas as invariantes são tratadas como unicas, portanto, ao validar o meta-modelo, todas as invariantes são verificadas consumindo tempo desnecessário porque mesmo se um falhar, os outros são testados.

####Flexibilidade limitada na definição de contexto
As invariantes OCL são definidas no contexto de meta-classes. 
Enquanto isso alcança um particionamento razoável do espaço do elemento do modelo, há casos em que é necessário um particionamento mais refinado.
Como a OCL suporta apenas o particionamento do espaço do elemento do modelo, meta-classes, invariantes devem aparecer sob o mesmo contexto (ou seja, Classe). Além disso, cada invariante deve explicitamente definir que ela aborda uam ou outra
sub-partição. 
Portanto, cada invariante deve limitar seu alcance inicialmente (usando o self.isA expressão) e depois expressar o corpo real.

####Não há suporte para reparar inconsistências
Embora a OCL possa ser usada para detectar inconsistências, ela não fornece nenhum meio para reparar
essas mesmas inconssitências. 
A razão é que a OCL foi concebida como uma linguagem livre de efeitos colaterais e não possui construções para modificar modelos. 
No entanto, existem muitos casos em que as inconsistências são triviais para resolver e os utilizadores podem beneficiar de reparação semi-automática das instalações.

####Não há suporte para restrições entre modelos
As expressões OCL (e, portanto, restrições OCL) só podem ser avaliadas no contexto de um modelo único de cada vez. 
Consequentemente, a OCL não pode ser usada para expressar restrições que em diferentes modelos. 
No contexto de um processo de engenharia conduzido por um modelo em grande escala que envolve muitos modelos diferentes (que potencialmente se adaptam a diferentes linguagens de
guages) esta limitação é particularmente severa.


###Sintaxe Abstrata da EVL

Na EVL, as especificações de validação são organizadas em módulos (EvlModule).
O EvlModule estende o EolLibraryModule, o que significa que ele pode conter
operações definidas e importar outros módulos de biblioteca EOL e módulos EVL.

####Contexto
Um contexto especifica o tipo de instâncias nas quais os invariantes contidas serão avaliadas. 
Cada contexto pode opcionalmente definir um guarda que limita a sua aplicabilidade a um subconjunto mais restrito de instâncias de seu tipo especificado.

####Invariante
Cada invariante da EVL define um nome e um corpo.
Opcionalmente, também pode definir um guarda (definido no seu supertipo GuardedElement abstrato) que limita ainda mais sua aplicabilidade a um subconjunto das instâncias do tipo definido por o contexto envolvente.
Cada invariante pode definir opcionalmente uma mensagem como ExpressionOrStatementBlock que deve retornar uma String fornecendo uma descrição do(s) motivo(s) para o qual a restrição falhou em um elemento em particular. 
Para suportar a reparação semi-automática de elementos nos quais as invariantes falharam.
Uma invariante pode definir opcionalmente várias correções.
Finalmente, uma invariante é uma classe abstrata que é usada como uma superclasse para os tipos específicos Restrição e Crítica.
Isso é para resolver a questão da separação de erros e advertências/críticas.

####Guarda
Guardas são usados para limitar a aplicabilidade das invariantes. 
Isso pode ser alcançado em dois níveis. 
No nível do Contexto, limita a aplicabilidade de todas as invariantes. E no nível Invariante, limita a aplicabilidade de uma invariante específica.

####Correção
Uma correção define um título usando um ExpressionOrStatementBlock em vez de uma String estática para
permitir que os utilizadores especifiquem títulos com reconhecimento de contexto (por exemplo, Rename class customer para Customer
de uma primeira letra convertida genérica para maiúscula).

####Restrição
Restrições na EVL são usadas para capturar erros críticos que invalidam modelo. 
Restrição é uma subclasse de Invariante e, portanto, herda todas as suas características.

####Crítica
As críticas são usadas para capturar situações não críticas que não invalida o modelo, mas deve, no entanto, ser tida em conta pelo utilizador para melhorar a qualidade do modelo.

####Pré e pós
Um módulo EVL pode definir um número de blocos pré e pós nomeados que contém instruções EOL que são executadas antes e depois de avaliar os invariantes respectivamente. 
Estes não devem ser confundidos com as anotações pré- / pós- capaz de operações definidas pelo utilizador EOL.

###Semântica de Execução

####Fase 1
Antes de qualquer invariante ser avaliada, as pré seções do módulo são executadas na ordem em que foram especificados.

####Fase 2
Para cada contexto non lazy com pelo menos uma invariante non lazy, as instâncias da meta-classe que define são obtidas. 
Para cada instância, o guarda do contexto é avaliado. 
Se o guarda estiver satisfeito, então para cada invariante non lazy contida no contexto, o guarda da invariante também é avaliado. 
Se a guarda da invariante estiver satisfeito, o corpo da invariante é avaliado. Caso o corpo seja avaliado como falso, a parte da mensagem da regra é avaliada e a mensagem produzida é adicionada juntamente com a instância, a
invariante e as correções disponíveis para o ValidationTrace.

####Fase 3
Nesta fase, a verificação de validação é examinada para restrições não satisfeitas e o utilizador é apresentado com a mensagem que cada um produziu. 
O utilizador pode então selecionar uma ou mais das correções disponíveis para serem executadas. 
A execução de correções é executada de uma forma transacional usando as respectivas facilidades fornecidas pelo modelo de conectividade de estrutura. 
Isso evita erros de tempo de execução durante a execução de uma correção que possa comprometer o modelo validado deixando-o num estado de inconsistência.

####Fase 4
Quando o utilizador executou todas as correções necessárias ou escolheu encerrar a Fase 3 explicitamente, a seção de post do módulo é executada. Lá, o utilizador pode executar tarefas como serializar a verificação de validação ou produzir um resumo do processo de validação dos resultados.

###Capturando Dependências Entre Invariantes

Para permitir que os utilizadores capturem tais dependências, a EVL fornece as operações internas booleanas satisfie(invariante:
String): Booleano, satisfiesAll (invariants: Sequence (String)): Booleano e satisfiesOne (invariantes: Sequence (String)). 
Utilizando essas operações, um invariante pode especificar no seu Guarda outras invariantes que precisam ser satisfeitas para o processo seja avaliado da melhor forma.

###Verificação de consistência intra-modelo

Cenário: O Padrão Singleton (comparando EVL com OCL)
O objetivo do caso de estudo é apresentar aos leitores a sintaxe concreta da linguagem e demonstrar os benefícios fornecidos pelos constructores adicionais que esta fornece.

#####Utilizando a OCL para expressar as invariantes
/ * filtre a cena 1 e 2 * /

####Utilizando a EVL para expressar as invariantes
/ * penho screenhoot 3 4 5 * /


###Verificação de consistência entre modelos

este exemplo demonstra o uso da EVL para detectar e reparar falhas ou contradições entre dois modelos diferentes.
/ * edom tela 6 7 8 * /

###Conclusão:

EVL fornece um número de recursos como suporte para feedback detalhado do utilizador, gestão de dependência de restrições, resolução de inconsistência transaccional semi-automática e (como é baseado no EOL) acesso para múltiplos modelos de diversos metamodelos e tecnologias.
## References

https://inis.iaea.org/collection/NCLCollectionStore/_Public/36/030/36030870.pdf?r=1&r=1 - Model Validation


In this folder you should add **all** artifacts developed for the investigative topic of the project.

There is only one folder for this task because it is a team task and, as such, usually there will be no need to create individual folders.

**Note:** If for some reason you need to bypass these guidelines please ask for directions with your teacher and **always** state the exceptions in your commits and issues in bitbucket.
